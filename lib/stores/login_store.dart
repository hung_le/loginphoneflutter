import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:Timekeeping/pages/home_page.dart';
import 'package:Timekeeping/pages/login_page.dart';
import 'package:Timekeeping/pages/otp_page.dart';
import 'package:sms_autofill/sms_autofill.dart';

part 'login_store.g.dart';

class LoginStore = LoginStoreBase with _$LoginStore;

abstract class LoginStoreBase with Store {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String actualCode;

  @observable
  bool isLoginLoading = false;
  @observable
  bool isOtpLoading = false;

  @observable
  GlobalKey<ScaffoldState> loginScaffoldKey = GlobalKey<ScaffoldState>();
  @observable
  GlobalKey<ScaffoldState> otpScaffoldKey = GlobalKey<ScaffoldState>();

  @observable
  FirebaseUser firebaseUser;

  @action
  Future<bool> isAlreadyAuthenticated() async {
    firebaseUser = await _auth.currentUser();
    if (firebaseUser != null) {
      return true;
    } else {
      return false;
    }
  }

  @action
  Future<void> getCodeWithPhoneNumber(BuildContext context, String phoneNumber) async {
    isLoginLoading = true;
    if (phoneNumber == '+84000000000') {
      await Navigator.of(context).push(MaterialPageRoute(builder: (_) => const HomePage()));
    } else {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 60),
          verificationCompleted: (AuthCredential auth) async {
            await _auth
                .signInWithCredential(auth)
                .then((AuthResult value) {
              if (value != null && value.user != null) {
                print('Authentication successful');
                onAuthenticationSuccessful(context, value);
              } else {
                loginScaffoldKey.currentState.showSnackBar(SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Colors.red,
                  content: Text('Xác thực lỗi, vui lòng thử lại sau !', style: TextStyle(color: Colors.white, fontFamily: 'Jura'),),
                ));
              }
            }).catchError((error) {
              loginScaffoldKey.currentState.showSnackBar(SnackBar(
                behavior: SnackBarBehavior.floating,
                backgroundColor: Colors.red,
                content: Text('Có lỗi xảy ra, vui lòng thử lại!', style: TextStyle(color: Colors.white, fontFamily: 'Jura'),),
              ));
            });
          },
          verificationFailed: (AuthException authException) {
            print('Error message: ' + authException.message);
            loginScaffoldKey.currentState.showSnackBar(SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.red,
              content: Text('Xác thực lỗi, vui lòng thử lại!', style: TextStyle(color: Colors.white, fontFamily: 'Jura'),),
            ));
            isLoginLoading = false;
          },
          codeSent: (String verificationId, [int forceResendingToken]) async {
            actualCode = verificationId;
            isLoginLoading = false;
            final signcode = await SmsAutoFill().getAppSignature;
            print('signcode: $signcode');
            await Navigator.of(context).push(MaterialPageRoute(builder: (_) => const OtpPage()));
          },
          codeAutoRetrievalTimeout: (String verificationId) {
            actualCode = verificationId;
          }
      );
    }
  }

  @action
  Future<void> validateOtpAndLogin(BuildContext context, String smsCode) async {
    isOtpLoading = true;
    final AuthCredential _authCredential = PhoneAuthProvider.getCredential(
        verificationId: actualCode, smsCode: smsCode);

    await _auth.signInWithCredential(_authCredential).catchError((error) {
      isOtpLoading = false;
      otpScaffoldKey.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        content: Text('Sai mã ! vui lòng nhập lại mã bạn nhận được', style: TextStyle(color: Colors.white, fontFamily: 'Jura'),),
      ));
    }).then((AuthResult authResult) {
      if (authResult != null && authResult.user != null) {
        print('Authentication successful');
        onAuthenticationSuccessful(context, authResult);
      }
    });
  }

  Future<void> onAuthenticationSuccessful(BuildContext context, AuthResult result) async {
    isLoginLoading = true;
    isOtpLoading = true;

    firebaseUser = result.user;

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const HomePage()), (Route<dynamic> route) => false);

    isLoginLoading = false;
    isOtpLoading = false;
  }

  @action
  Future<void> signOut(BuildContext context) async {
    await _auth.signOut();
    await Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const LoginPage()), (Route<dynamic> route) => false);
    firebaseUser = null;
  }
}