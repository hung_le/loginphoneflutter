import 'dart:convert';

import 'package:Timekeeping/model/NotificationModel.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:Timekeeping/stores/login_store.dart';
import 'package:Timekeeping/theme.dart';
import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'dart:developer' as dev;
import 'dart:io' show Platform;


// Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
//   dev.debugger();
//   if (message.containsKey('data')) {
//     // Handle data message
//     final dynamic data = message['data'];
//     final image = data['image_url'];
//     final title = data['body'];
//     final currentTask = TaskModel(name: title, image:  image);
//     await TodoHelper().insertTask(currentTask);
//   }
//   if (message.containsKey('notification')) {
//     // Handle notification message
//     final dynamic notification = message['notification'];
//     final title = notification['body'];
//     final image = message['data']['image_url'];
//     final currentTask = TaskModel(name: title, image:  image);
//     await TodoHelper().insertTask(currentTask);
//   }
//   print('AppPushs myBackgroundMessageHandler : $message');
//   return Future<void>.value();
//
// }

final Map<String, Item> _items = <String, Item>{};

Item _itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['notification'] ?? message;
  final String itemId = message['notification'];
  final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
    .._name = data['body']
    .._time = data['title'];
  return item;
}

Future<void> saveIntInLocalMemory(String key, String value) async {
  var pref = await SharedPreferences.getInstance();
  pref.setString(key, value);
}

class Item {
  Item({this.itemId});

  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();

  Stream<Item> get onChanged => _controller.stream;

  String _name;

  String get name => _name;

  set name(String value) {
    _name = value;
    _controller.add(this);
  }

  String _time;

  String get time => _time;

  set time(String value) {
    _time = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};

  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => DetailPage(itemId),
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage(this.itemId);

  final String itemId;

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Item _item;
  StreamSubscription<Item> _subscription;


  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId];
    _subscription = _item.onChanged.listen((Item item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Match ID ${_item.itemId}"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Card(
            child: Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      child: Column(
                        children: <Widget>[
                          Text('',
                              style: TextStyle(
                                  color: Colors.black.withOpacity(0.8))),
                          Text(_item.time,
                              style: Theme.of(context).textTheme.title)
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      child: Column(
                        children: <Widget>[
                          Text('',
                              style: TextStyle(
                                  color: Colors.black.withOpacity(0.8))),
                          Text(_item.name,
                              style: Theme.of(context).textTheme.title)
                        ],
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _topicButtonsDisabled = false;
  final TodoHelper _todoHelper = TodoHelper();
  List<TaskModel> tasks = [];
  TaskModel currentTask;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final TextEditingController _topicController =
      TextEditingController(text: 'topic');
  String _message = '';
  String _image = '';

  Widget _buildDialog(BuildContext context, Item item) {
    return AlertDialog(
      content: Text("${item.name}"),
      actions: <Widget>[
        FlatButton(
          child: const Text('ĐÓNG'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('MỞ'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  Future<void> getNoti() async {
    List<TaskModel> list = await _todoHelper.getAllTask();
    setState(() {
      tasks = list;
      print('noti ${tasks.first.name}');
    });
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  Future<String> getIntFromLocalMemory(String key) async {
    var pref = await SharedPreferences.getInstance();
    var number = pref.getString(key) ?? 0;
    return number;
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    // dev.debugger();
    // if (message.containsKey('data')) {
    //   // Handle data message
    //   final dynamic data = message['data'];
    //   final image = data['image_url'];
    //   final title = data['body'];
    //   final currentTask = TaskModel(name: title, image:  image);
    //   await TodoHelper().insertTask(currentTask);
    // }
    // if (message.containsKey('notification')) {
    //   // Handle notification message
    //   final dynamic notification = message['notification'];
    //   final title = notification['body'];
    //   final image = message['data']['image_url'];
    //   final currentTask = TaskModel(name: title, image:  image);
    //   await TodoHelper().insertTask(currentTask);
    // }
    // print('AppPushs myBackgroundMessageHandler : $message');
    // return Future<void>.value();
    print('AppPushs myBackgroundMessageHandler : $message');
    var currentTask;
    if (Platform.isAndroid) {
        final dynamic data = message['data'];
        final image = data['image_url'];
        final title = data['body'];
        currentTask = TaskModel(name: title, image:  image);
        await TodoHelper().insertTask(currentTask);
    } else {
      final dynamic data = message['data'];
      final image = data['image_url'];
      final title = data['body'];
      currentTask = TaskModel(name: title, image:  image);
      await TodoHelper().insertTask(currentTask);
    }
    return Future<void>.value();

  }

  @override
  void initState() {
    //getNoti();
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //await _showItemDialog(message);
        setState(() => _message = message["notification"]["body"]);
        setState(() => _image = message["data"]["image_url"]);
        print("image url: $_image");
        currentTask = TaskModel(name: _message, image:  _image);
        _todoHelper.insertTask(currentTask);
        await getNoti();
      },
      onBackgroundMessage: Platform.isAndroid ? myBackgroundMessageHandler : null,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //await _navigateToItemDetail(message);
        setState(() => _message = message["data"]["body"]);
        setState(() => _image = message["data"]["image_url"]);
        print("image on La$_image");
        currentTask = TaskModel(name: _message, image:  _image);
        _todoHelper.insertTask(currentTask);
        await getNoti();
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        //await _navigateToItemDetail(message);
        setState(() => _message = message["data"]["body"]);
        setState(() => _image = message["data"]["image_url"]);
        print("image on resume: $_image");
        currentTask = TaskModel(name: _message, image:  _image);
        _todoHelper.insertTask(currentTask);
        await getNoti();
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("Push Messaging token: $token");
    });
    getIntFromLocalMemory('PHONE_NUMBER').then(
        (value) => _firebaseMessaging.subscribeToTopic(value).then((token) {
              print("Topic Subscribed Successfully");
              getNoti();
            }).catchError((onError) {
              print("error subscribing to topic ${onError.toString()}");
            }).catchError((onError) {
              print("Error in fcm token ${onError.toString()}");
            }));
  }

  @override
  Widget build(BuildContext context) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM/yyyy hh:mm a');
    final String formatted = formatter.format(now);
    return Consumer<LoginStore>(
      builder: (_, loginStore, __ ) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: const Text('Camera AI Face',
                style: TextStyle(fontFamily: 'Jura')),
          ),
          backgroundColor: Colors.white,
          body: tasks.isNotEmpty ?
          Column(children: <Widget>[
            Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: const Text(
                    'Hệ thống sẽ tự động gửi thông báo về thiết bị này khi chấm công thành công,'
                    ' nếu không muốn nhận thông báo, vui lòng đăng xuất khỏi thiết bị bằng cách nhấn vào nút phía dưới, Xin cám ơn!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'Jura',
                        color: MyColors.primaryColorLight),
                  ),
                )
              ],
            ),
            Expanded(child: ListView.separated(itemBuilder: (context, index)  {
              //getNoti();
              return tasks.isNotEmpty ? ListTile(
                leading: CircleAvatar(
                  backgroundImage: (tasks[index].image == null) ? AssetImage('assets/img/MobifoneAI.jpg') : NetworkImage("${tasks[index].image}"), // no matter how big it is, it won't overflow
                ),
                title: Text("${tasks[index].name}", style: TextStyle(fontSize: 16, fontFamily: 'Jura', color: MyColors.colorTextNoti)),
              ) : Column(
                children: [
                  Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Image.asset(
                      'assets/img/MobifoneAI.jpg',
                      fit: BoxFit.cover,
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    elevation: 5,
                    margin: EdgeInsets.all(10.0),
                  ),
                ],
              );
            },
                separatorBuilder: (context, index) => Divider(),
                itemCount: tasks.length,)),


            const Padding(padding: EdgeInsets.only(bottom: 50)),
          ]) : Column(children: <Widget>[
            Column(
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(10.0),
                  child: const Text(
                    'Hệ thống sẽ tự động gửi thông báo về thiết bị này khi chấm công thành công,'
                        ' nếu không muốn nhận thông báo, vui lòng đăng xuất khỏi thiết bị bằng cách nhấn vào nút phía dưới, Xin cám ơn!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'Jura',
                        color: MyColors.primaryColorLight),
                  ),
                )
              ],
            ),
             Column(
                children: [
                  Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Image.asset(
                      'assets/img/MobifoneAI.jpg',
                      fit: BoxFit.cover,
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    elevation: 5,
                    margin: EdgeInsets.all(10.0),
                  ),
                ],
              ),
          ]),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.logout),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                          title: Text('Đăng xuất', style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Jura',
                              color:
                              MyColors.primaryColor)),
                          content: Text('Bạn có chắc chắn muốn đăng xuất?', style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Jura',
                              color:
                              MyColors.primaryColor)),
                          actions: <Widget>[
                            FlatButton(
                                child: Text('Huỷ', style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Jura',
                                    color:
                                    Colors.red)),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                }),
                            FlatButton(
                                child: Text('Đồng ý', style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Jura',
                                    color:
                                    MyColors.primaryColor)),
                                onPressed: () {
                                  loginStore.signOut(context);
                                  getIntFromLocalMemory('PHONE_NUMBER').then(
                                      (value) => _firebaseMessaging
                                          .unsubscribeFromTopic(value));
                                })
                          ],
                        ));
              }),
        );
      },
    );
  }
}
