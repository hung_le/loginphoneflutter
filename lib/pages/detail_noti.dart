import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:Timekeeping/pages/home_page.dart';
import 'package:Timekeeping/pages/login_page.dart';
import 'package:Timekeeping/stores/login_store.dart';
import 'package:Timekeeping/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailNoti extends StatefulWidget {
  const DetailNoti({Key key}) : super(key: key);
  @override
  _DetailNotiState createState() => _DetailNotiState();
}

class _DetailNotiState extends State<DetailNoti> {


  @override
  void initState() {
    super.initState();
    Provider.of<LoginStore>(context, listen: false).isAlreadyAuthenticated().then((result) {
      if (result) {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const HomePage()), (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const LoginPage()), (Route<dynamic> route) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primaryColor,
    );
  }
}
