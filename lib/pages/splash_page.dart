import 'package:Timekeeping/pages/detail_noti.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:Timekeeping/pages/home_page.dart';
import 'package:Timekeeping/pages/login_page.dart';
import 'package:Timekeeping/stores/login_store.dart';
import 'package:Timekeeping/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key key}) : super(key: key);
  @override
  _SplashPageState createState() => _SplashPageState();
}

Future<String> getIntFromLocalMemory(String key) async {
  var pref = await SharedPreferences.getInstance();
  var number = pref.getString(key) ?? 0;
  return number;
}

class _SplashPageState extends State<SplashPage> {
  var valueVisit = getIntFromLocalMemory('isHasVisit');

  @override
  void initState() {
    super.initState();
    Provider.of<LoginStore>(context, listen: false).isAlreadyAuthenticated().then((result) {
      if (result) {
        //if(valueVisit == '1') {
        //  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const DetailNoti()), (Route<dynamic> route) => false);
        //} else {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const HomePage()), (Route<dynamic> route) => false);
       // }
      } else {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const LoginPage()), (Route<dynamic> route) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primaryColor,
    );
  }
}
