import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

final String tableName = 'notification';
final String Column_id = 'id';
final String Column_name = 'name';
final String Column_image = 'image';

class TaskModel{
  final String name;
  final String image;
  int id;

  TaskModel({this.name, this.image, this.id});

  Map<String, dynamic> toMap(){
    return {
      Column_name : this.name,
      Column_image: this.image
    };
  }
}

class TodoHelper{
  Database db;

  TodoHelper(){
    initDatabase();
  }

  Future<void> initDatabase() async{
    db = await openDatabase(
        join(await getDatabasesPath(), "database.db"),
        onCreate: (db, version){
          return db.execute("CREATE TABLE $tableName($Column_id INTEGER PRIMARY KEY AUTOINCREMENT, $Column_name TEXT, $Column_image TEXT)");
        },
        version: 1
    );
  }

  Future<void> insertTask(TaskModel task) async{
    try{
      db.insert(tableName, task.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
      print('insert success');
    }catch(_){
      print("có lỗi");
    }
  }

  Future<List<TaskModel>> getAllTask () async{
    final List<Map<String, dynamic>> tasks = await db.query(tableName.toString(), orderBy: '$Column_id DESC');//await db.query(tableName.toString());

    return List.generate(tasks.length, (i){
      return TaskModel(name: tasks[i][Column_name], image: tasks[i][Column_image], id: tasks[i][Column_id]);
    });
  }
}
