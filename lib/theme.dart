import 'package:flutter/material.dart';

class MyColors {
  static const Color primaryColor = Color(0xFF3300FF);
  static const Color primaryColorLight = Color(0xFF3366CC);
  static const Color colorTextNoti = Color(0xFF555555);
}